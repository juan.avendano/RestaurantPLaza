package com.example.RestaurantPLaza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestaurantPLazaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestaurantPLazaApplication.class, args);
	}

}
