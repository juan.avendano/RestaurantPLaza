package com.example.RestaurantPLaza.infrastructure.exception;

public class OwnerAlreadyExistException extends RuntimeException{
    public OwnerAlreadyExistException(String message) {
        super(message);
    }



}
